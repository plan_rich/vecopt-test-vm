""" Execute ./r-c [--jit op1=X:op2=Y:...] <filename>
"""

import sys
from rpython.rlib.streamio import open_file_as_stream
from rpython.jit.codewriter.policy import JitPolicy
from rpython.rlib import jit
from rrr.interpreter import interpret

def main(argv):
    if argv[1] == "--jit" and len(argv) == 4:
        for entry in argv[2].split(':'):
            jit.set_user_param(None, entry)
        path = argv[3]
    elif not len(argv) == 2:
        print __doc__
        return 1
    else:
        path = argv[1]
    f = open_file_as_stream(path)
    data = f.readall()
    f.close()
    interpret(data)
    return 0

def target(driver, args):
    return main, None

def jitpolicy(driver):
    return JitPolicy()

if __name__ == '__main__':
    main(sys.argv)
