
# a 16 * 16 grid consisting of 16 attributes
attrs <- 16;
gridx <- 16;
gridy <- 16;
elemCount <- attrs * gridx * gridy;
weightVectors <- array(elemCount);
i <- 0;
bmu <- array(attrs);
e <- 0.8;
while (i != 1000) {
    rv <- bmu - weightVectors[i:i+16];
    dst <- distance(rv);
    e <- e - (e/2);
    m <- dst * e
    weightVectors[i:i+16] <- bmu + rv * m
    i <- i + 1;
}
