
"""This file contains both an interpreter and "hints" in the interpreter code 
necessary to construct a Jit.

There are two required hints:
1. JitDriver.jit_merge_point() at the start of the opcode dispatch loop
2. JitDriver.can_enter_jit() at the end of loops (where they jump back to the start)

These bounds and the "green" variables effectively mark loops and 
allow the jit to decide if a loop is "hot" and in need of compiling.

Read http://doc.pypy.org/en/latest/jit/pyjitpl5.html for details.

"""

from rrr.sourceparser import parse
from rrr.bytecode import compile_ast
from rrr import bytecode
from rpython.rlib import jit, rgc
from rpython.rlib.rawstorage import (alloc_raw_storage, raw_storage_setitem,
                                     free_raw_storage, raw_storage_getitem)
from rpython.rtyper.lltypesystem import lltype, rffi
from rpython.rlib.rarithmetic import intmask
from rpython.rlib.unroll import unrolling_iterable

def printable_loc(pc, code, bc):
    return str(pc) + " " + bytecode.bytecodes[ord(code[pc])]

driver = jit.JitDriver(greens = ['pc', 'code', 'bc'],
                       reds = ['frame'],
                       virtualizables=['frame'],
                       get_printable_location=printable_loc)

class W_Root(object):
    pass

class W_IntObject(W_Root):
    def __init__(self, intval):
        assert(isinstance(intval, int))
        self.intval = intval

    def add(self, other):
        if not isinstance(other, W_IntObject):
            raise Exception("wrong type")
        return W_IntObject(self.intval + other.intval)

    def lt(self, other): 
        if not isinstance(other, W_IntObject):
            raise Exception("wrong type")
        return W_IntObject(self.intval < other.intval)

    def is_true(self):
        return self.intval != 0

    def as_int(self):
        return self.intval

    def str(self):
        return str(self.intval)

class W_RangeObject(W_Root):
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.size = end-start 

    def getsize(self):
        return self.size

    def getindex(self):
        assert not self.is_range()
        return self.start

    def is_range(self):
        return self.start != self.end

    def getbegin(self):
        return self.start

    def getend(self):
        return self.end

    def str(self):
        return '<range [%d-%d)>' % (self.start, self.end)

vector_driver = jit.JitDriver(greens = [],
                       reds = ['i','size', 'va','vb','vc'],
                       vectorize = True)
def ufunc_add(a, b, c):
    va = a.array
    vb = b.array
    vc = c.array
    size = a.size
    i = 0
    while i < size:
        vector_driver.can_enter_jit(
                i=i, size=size,
                va=va, vb=vb, vc=vc)
        vector_driver.jit_merge_point(
                i=i, size=size,
                va=va, vb=vb, vc=vc)
        ea = va[i]
        eb = vb[i]
        ec = rffi.r_int(intmask(ea)+intmask(eb))
        vc[i] = ec
        i += 1

class W_ArrayObjectBase(W_Root):
    def get(self, index):
        raise NotImplementedError
    def set(self, index, value):
        raise NotImplementedError
    def add(self, other):
        raise NotImplementedError

class W_ArrayObject(W_Root):
    def __init__(self, w_range, array):
        self.size = w_range.getsize()
        self.array = array
        if w_range.is_range():
            arr = self.array
            for i in range(w_range.getbegin(), w_range.getend()):
                self.array[i] = rffi.r_int(i)

    @rgc.must_be_light_finalizer
    def __del__(self):
        lltype.free(self.array, flavor='raw', track_allocation=False)

    def add(self, other):
        if not isinstance(other, W_ArrayObject):
            raise Exception("wrong type")
        ufunc_add(self, other, self)
        return self

    def get(self, index):
        if index.is_range():
            raise NotImplementedError
        p_i = intmask(self.array[index.getindex()])
        return W_IntObject(p_i)

    def set(self, w_range, w_value):
        if w_range.is_range():
            raise NotImplementedError
        i = rffi.r_int(w_value.as_int())
        self.array[w_range.getindex()] = i

    def str(self):
        return "<array size: %s>" % self.size

T = lltype.Array(rffi.INT, hints={'nolength':True})

class ArrayType(object):
    def __init__(self, itemtype):
        self.itemtype = itemtype
        self.w_class = None

    def _freeze_(self):
        # hint for the annotator: track individual constant instances
        return True

def wrap_array(code,w_range):
    mytype = ArrayType(rffi.INT)
    array = lltype.malloc(T, w_range.getsize(), flavor='raw',
              track_allocation=False, add_memory_pressure=True)
    return W_ArrayObject(w_range, array)

class W_FloatObject(W_Root):
    def __init__(self, floatval):
        assert(isinstance(floatval, float))
        self.floatval = floatval

    def add(self, other):
        if not isinstance(other, W_FloatObject):
            raise Exception("wrong type")
        return W_FloatObject(self.floatval + other.floatval)

    def lt(self, other): 
        if not isinstance(other, W_FloatObject):
            raise Exception("wrong type")
        return W_IntObject(self.floatval < other.floatval)

    def str(self):
        return str(self.floatval)

class Frame(object):
    _virtualizable_ = ['valuestack[*]', 'valuestack_pos', 'vars[*]']
    
    def __init__(self, bc):
        self = jit.hint(self, fresh_virtualizable=True, access_directly=True)
        self.valuestack = [None] * 4 # safe estimate!
        self.vars = [None] * bc.numvars
        self.valuestack_pos = 0

    def push(self, v):
        pos = jit.hint(self.valuestack_pos, promote=True)
        assert pos >= 0
        self.valuestack[pos] = v
        self.valuestack_pos = pos + 1

    def pop(self):
        pos = jit.hint(self.valuestack_pos, promote=True)
        new_pos = pos - 1
        assert new_pos >= 0
        v = self.valuestack[new_pos]
        self.valuestack_pos = new_pos
        return v

def add(left, right):
    return left + right

def execute(frame, bc):
    code = bc.code
    pc = 0
    while True:
        # required hint indicating this is the top of the opcode dispatch
        driver.jit_merge_point(pc=pc, code=code, bc=bc, frame=frame)
        c = ord(code[pc])
        arg = ord(code[pc + 1])
        pc += 2
        if c == bytecode.LOAD_CONSTANT:
            w_constant = bc.constants[arg]
            frame.push(w_constant)
        elif c == bytecode.DISCARD_TOP:
            frame.pop()
        elif c == bytecode.RETURN:
            return
        elif c == bytecode.BINARY_ADD:
            right = frame.pop()
            left = frame.pop()
            w_res = left.add(right)
            frame.push(w_res)
        elif c == bytecode.BINARY_LT:
            right = frame.pop()
            left = frame.pop()
            frame.push(left.lt(right))
        elif c == bytecode.JUMP_IF_FALSE:
            if not frame.pop().is_true():
                pc = arg
        elif c == bytecode.JUMP_BACKWARD:
            pc = arg
            # required hint indicating this is the end of a loop
            driver.can_enter_jit(pc=pc, code=code, bc=bc, frame=frame)
        elif c == bytecode.PRINT:
            item = frame.pop()
            print item.str()
        elif c == bytecode.ASSIGN:
            frame.vars[arg] = frame.pop()
        elif c == bytecode.LOAD_VAR:
            frame.push(frame.vars[arg])
        elif c == bytecode.ARRAY_NEW:
            w_init = frame.pop()
            frame.push(wrap_array('i', w_init))
        elif c == bytecode.ARRAY_GET:
            w_index = frame.pop()
            w_array = frame.vars[arg]
            frame.push(w_array.get(w_index))
        elif c == bytecode.ARRAY_SET:
            w_index = frame.pop()
            w_value = frame.pop()
            w_array = frame.vars[arg]
            w_array.set(w_index, w_value)
        else:
            assert False
def code_int16(code, pc):
    return (ord(code[pc]) & 0xff) & (ord(code[pc+1]) & 0xff) << 8
def interpret(source):
    import time
    bc = compile_ast(parse(source))
    #print bc.dump()
    start = time.clock()
    frame = Frame(bc)
    execute(frame, bc)
    end = time.clock()
    print "elapsed clock: %f" % (end - start)
    return frame # for tests and later introspection
