import py
from rrr.sourceparser import parse, Stmt, Block, ConstantInt, ConstantFloat, BinOp,\
     Variable, Assignment, While, If, Print, Range, Name

def test_parse_basic():
    assert parse('13;') == Block([Stmt(ConstantInt(13))])
    assert parse('1 + 2;') == Block([Stmt(BinOp("+", ConstantInt(1),
                                                ConstantInt(2)))])
    assert parse('1 + a;') == Block([Stmt(BinOp('+', ConstantInt(1),
                                                Variable('a')))])

def test_float():
    assert parse('1.0;') == Block([Stmt(ConstantFloat(1.0))])
    assert parse('0.5;') == Block([Stmt(ConstantFloat(0.5))])
    assert parse('0.0;') == Block([Stmt(ConstantFloat(0.0))])
    assert parse('-1.0;') == Block([Stmt(ConstantFloat(-1.0))])
    assert parse('10.0;') == Block([Stmt(ConstantFloat(10.0))])
    assert parse('.1;') == Block([Stmt(ConstantFloat(.1))])
    assert parse('1.0e5;') == Block([Stmt(ConstantFloat(1.0e5))])
    assert parse('1.0E-5;') == Block([Stmt(ConstantFloat(1.0E-5))])
    assert parse('1.0e+11;') == Block([Stmt(ConstantFloat(1.0e11))])

def test_multiple_statements():
    assert parse('''
    1 + 2;
    c;
    e;
    ''') == Block([Stmt(BinOp("+", ConstantInt(1), ConstantInt(2))),
                   Stmt(Variable('c')),
                   Stmt(Variable('e'))])

def test_assignment():
    assert parse('a <- 3;') == Block([Assignment(Name('a'), ConstantInt(3))])

def test_while():
    assert parse('while (1) { a <- 3; }') == Block([While(ConstantInt(1),
              Block([Assignment(Name('a'), ConstantInt(3))]))])

def test_if():
    assert parse("if (1) { a; }") == Block([If(ConstantInt(1),
                                               Block([Stmt(Variable("a"))]))])

def test_print():
    assert parse("print(x);") == Block([Stmt(Print(Variable("x")))])

def test_range():
    assert parse("a <- 0:10;") == Block([Assignment(Name('a'), Range(0,10))])

def test_var_range():
    assert parse("x <- 0; y <- 10; a <- x:y;") == Block([Assignment(Name('a'), Range(Variable(x),Variable(y)))])

def test_array_index():
    parse("x <- 0; y <- array(1:100); y[x] <- y[1] + 1; print(y[0]);")
