import py
from rrr.interpreter import interpret

def test_interp():
    interpret('1 + 2;')
    # nothing to assert

def test_print(capfd):
    interpret('print(1);')
    out, err = capfd.readouterr()
    assert out == '1\n'

def test_float_add(capfd):
    interpret('print(1.5 + .5);')
    out, err = capfd.readouterr()
    assert out == '2.0\n' and not err

def test_float_lt(capfd):
    interpret('print(1.5 < .5);')
    out, err = capfd.readouterr()
    assert out == 'False\n' and not err

def test_while(capfd):
    interpret("""
      n <- 0;
      while (n < 10) { 
         n <- n + 1; 
      }
      print(n);
    """)
    out, err = capfd.readouterr()
    assert out == '10\n'

def test_if(capfd):
    interpret('''
    if (1) {
       print(2);
    }
    ''')
    out, err = capfd.readouterr()
    assert out == '2\n'

def test_range(capfd):
    interpret('a <- array(0:10); print(a);')
    out, err = capfd.readouterr()
    assert out == '<array size: 10>\n'

def test_range_add(capfd):
    interpret('a <- array(0:10); b <- array(0:10); print(a+b);')
    out, err = capfd.readouterr()
    assert out == '<array size: 10>\n'

def test_range_get(capfd):
    interpret('a <- 0:10; a[0] <- 42; print(a[0]); print(a[9]);')
    out, err = capfd.readouterr()
    assert out == '42\n9\n'

def test_range_get(capfd):
    code = """
    a <- array(0:5);
    a[1] <- a[2] + a[3] + 500 + a[1];
    print(a[1]);
    """
    interpret(code)
    out, err = capfd.readouterr()
    assert out == '506\n'
