
from rrr.interpreter import interpret, compile_ast, parse, Frame, execute

class JitBaseTest(object):
    def test_jit(self):
        codes = ['print(1);', 'n <- 0; while (n < 10) { n <- n + 1; }']
        def main(i):
            interpret(codes[i])
        self.meta_interp(main, [1], listops=True)

    def test_vec_add(self):
        code = """
        n <- array(0:100);
        n <- n+n;
        print(n[97]);
        print(n[98]);
        print(n[99]);
        """
        def main():
            bc = compile_ast(parse(code))
            frame = Frame(bc)
            execute(frame, bc)
        self.meta_interp(main, [], listops=True)
