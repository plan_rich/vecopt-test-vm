
from rpython.jit.backend.x86.test.test_basic import Jit386Mixin
from rrr.interpreter import interpret, compile_ast, parse, Frame, execute
from rrr.test.test_jit import JitBaseTest

class TestJit(Jit386Mixin, JitBaseTest):
    pass
