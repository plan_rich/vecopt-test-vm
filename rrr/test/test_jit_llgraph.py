
from rpython.jit.metainterp.test.support import LLJitMixin
from rrr.interpreter import interpret, compile_ast, parse, Frame, execute
from rrr.test.test_jit import JitBaseTest

class TestLLGraph(LLJitMixin, JitBaseTest):
    pass
